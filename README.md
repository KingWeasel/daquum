# Everything

Class door combo: 3, 4&5 at same time, 1

## Week 1
### What I planned
- Get data from ADS1278 using Arduino 
### What got Done
- Verified Arduino SPI working through level shifter using ADXL345 accelerometer

## Notes
### Arduino UNO
- MOSI -> 11
- MISO -> 12
- SCLK -> 13
- DRDY -> 10