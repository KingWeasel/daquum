/*
 * Source Modified by Derek Molloy for Exploring BeagleBone Rev2
 * Based on the examples distributed by TI
 *
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *	* Redistributions of source code must retain the above copyright
 *	  notice, this list of conditions and the following disclaimer.
 *
 *	* Redistributions in binary form must reproduce the above copyright
 *	  notice, this list of conditions and the following disclaimer in the
 *	  documentation and/or other materials provided with the
 *	  distribution.
 *
 *	* Neither the name of Texas Instruments Incorporated nor the names of
 *	  its contributors may be used to endorse or promote products derived
 *	  from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

 /*
 */ 

#include <string.h>
#include <stdint.h>
#include <pru_cfg.h>
#include <pru_ctrl.h>
#include "resource_table_empty.h"

#define FULL       0xFFFFFFFF
#define SYNC_ON    0x00000002 // 0010
#define SYNC_OFF   0xFFFFFFFD // 1101
#define SCLK_ON    0x00000004 // 0100
#define SCLK_OFF   0xFFFFFFFB // 1011
#define LO_BIT     0x00000001
#define LO_BYTE    0x000000FF
#define SHRAM      0x00010000
#define CTRL       0x00024000
#define CNTR_EN    0x8
#define CNTR_REG   0xC

volatile register uint32_t __R30;  //32 output gpios
volatile register uint32_t __R31;  //32 input gpios
volatile uint32_t          bit, drdy, sign, cycles, sample[8]; 
volatile uint8_t           bit_per_chan[24];
volatile int8_t            i;

void main(void)
{
    __R30 &= SCLK_OFF; 

    __R30 &= SYNC_OFF;  // SYNC goes lo to enable
    __delay_cycles(100);
    __R30 |= SYNC_ON;  

    PRU1_CTRL.CTRL_bit.CTR_EN = 1;

    while(1) 
    {
//        if(PRU1_CTRL.CTRL_bit.CTR_EN == 0)
//        {
//            PRU1_CTRL.CTRL_bit.CTR_EN = 1;
//        }

        // wait for DRDY bit to clear, slightly faster 
        // response than polling at the C level
        // needs at least one space in front of instruction
        asm(" WBC r31, 0");

        // get timestamp code goes here
        // t0 logic goes here

        // adc puts out 24-bit signed value
        for(i = 23; i >= 0; i--)
        {
            __R30 |= SCLK_ON; // tick
           bit_per_chan[i] = (__R31 >> 3) & LO_BYTE;
            __R30 &= SCLK_OFF; // tock
        }

        // shove samples to memory with timestamp code
        // goes here
    }
}

