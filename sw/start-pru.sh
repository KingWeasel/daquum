#!/bin/bash

cpupower -c all frequency-set -g performance

#using PRU1
echo stop > /sys/class/remoteproc/remoteproc2/state
sleep 1
echo start > /sys/class/remoteproc/remoteproc2/state

 
config-pin P8_45 pruin  # ~DRDY
config-pin P8_46 pruout # ~SYNC
config-pin P8_43 pruout # SCLK
config-pin P8_44 pruin  # D1
config-pin P8_41 pruin  # D2 
config-pin P8_42 pruin  # D3
#config-pin P8_42 pruout  # D3
config-pin P8_39 pruin  # D4
config-pin P8_40 pruin  # D5
config-pin P8_27 pruin  # D6
config-pin P8_29 pruin  # D7
config-pin P8_28 pruin  # D8
# config-pin P8_30 pruin # Not used 

# eMMC uses these
#config-pin P8_21 pruout
#config-pin P8_20 pruout

