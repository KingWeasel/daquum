#define DRDYPin 8
#define SCLKPin 9
#define MISOPin 10

volatile int32_t sample;
volatile uint32_t tmp, bit, sign;
volatile int i, j;
volatile float conversion;

// yanking from wiring_digital.c and pre-computing some addresses
EPortType SCLKport = g_APinDescription[SCLKPin].ulPort;
uint32_t SCLKpin = g_APinDescription[SCLKPin].ulPin;
uint32_t SCLKpinMask = (1ul << SCLKpin);
volatile uint32_t *SCLKSETREG = &PORT->Group[SCLKport].OUTSET.reg;
volatile uint32_t *SCLKCLRREG = &PORT->Group[SCLKport].OUTCLR.reg;

// same for MISO
EPortType MISOport = g_APinDescription[MISOPin].ulPort;
uint32_t MISOpin = g_APinDescription[MISOPin].ulPin;
volatile uint32_t *MISOINREG = &PORT->Group[MISOport].IN.reg;

// ...and DRDY
EPortType DRDYport = g_APinDescription[DRDYPin].ulPort;
uint32_t DRDYpin = g_APinDescription[DRDYPin].ulPin;
volatile uint32_t *DRDYINREG = &PORT->Group[DRDYport].IN.reg;

void setup()
{
  Serial.begin(2000000);

  pinMode(SCLKPin, OUTPUT);
  pinMode(MISOPin, INPUT);
  pinMode(DRDYPin, INPUT_PULLUP);

  // set SCLK = 0
  PORT->Group[SCLKport].OUTCLR.reg = SCLKpinMask;

  // setup interrupt
  attachInterrupt(digitalPinToInterrupt(DRDYPin), DRDY, FALLING);
}

void loop(){}

void DRDY()
{
  tmp = 0;
  // prime the pump
  SCLK(1);
  SCLK(0);

  for (i = 23; i >= 0; i--)
  {
    SCLK(1); // tick
    bit = ReadMISO();
    SCLK(0); // tock
    if(i)
      tmp = (tmp | bit) << 1;
  }

  sign = (tmp >> 23) ? 0xFF000000 : 0x00000000;
  sample = int32_t(sign | tmp);

  j++;
  // low frequency plot update
  if (j >= 250)
  {
    j = 0;
    conversion = (2.5 / (pow(2, 23) - 1)) * sample;
    Serial.println(conversion, 9);
  }
}

void SCLK(byte value)
{
  if (value)
    *SCLKSETREG = SCLKpinMask;
  else
    *SCLKCLRREG = SCLKpinMask;
}

byte ReadMISO()
{
  return (*MISOINREG & (1ul << MISOpin)) ? HIGH : LOW;
}

byte ReadDRDY()
{
  return (*DRDYINREG & (1ul << DRDYpin)) ? HIGH : LOW;
}