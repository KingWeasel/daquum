const byte inPin = 13;
const byte SCLKPin = 9;
const byte MISOPin = 10;
volatile byte bleep = LOW;
volatile byte clk_level = LOW;

void setup()
{
    Serial.begin(9600);

    pinMode(inPin, INPUT_PULLDOWN);
    pinMode(SCLKPin, OUTPUT);
    pinMode(MISOPin, INPUT);
}

void loop()
{
    clk_level = !clk_level;
    digitalWrite(SCLKPin, clk_level);
    Serial.println(digitalRead(inPin));
}
