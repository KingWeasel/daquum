const byte interruptPin = 8;
volatile byte bleep = LOW;

void setup()
{
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);

  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), data_ready, FALLING);
}

void loop()
{
  bleep = !bleep;
  delay(100);
}

void data_ready()
{
  digitalWrite(LED_BUILTIN, bleep);
}