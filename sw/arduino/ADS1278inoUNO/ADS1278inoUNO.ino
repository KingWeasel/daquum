#include <SPI.h>

#define DRDYPin 3

volatile int32_t sample, MISObit, sign;

void setup()
{
    // Serial.begin(115200);

    // setup interrupt
    pinMode(DRDYPin, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(DRDYPin), DRDY, FALLING);

    // 14-15 = X
    //13 = OUTPUT
    // 12 = INPUT
    // 8-11 = X
    DDRB = DDRB | B00100000;
}

void loop()
{
    Serial.println(sample);
}

void DRDY()
{
    sample = 0;

    for(int i = 23; i >= 0; i--)
    {
        PORTB = 0x20;// tick
        MISObit = (PIND >> 4) & 0x01;// get bit 4
        PORTB = 0x00;// tock
        sample = (sample << 1) | MISObit;       
    }

    // sign = (sample >> 23) & 0x01;// get bit 23
    // // sample = sign ? (sample | 0xFF000000) : (sample | 0x00000000);
    // sample = sample | (sign ? 0xFF000000 : 0x00000000);
}
