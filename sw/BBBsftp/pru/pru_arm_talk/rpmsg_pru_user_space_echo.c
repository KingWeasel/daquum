/*
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *	* Redistributions of source code must retain the above copyright
 *	  notice, this list of conditions and the following disclaimer.
 *
 *	* Redistributions in binary form must reproduce the above copyright
 *	  notice, this list of conditions and the following disclaimer in the
 *	  documentation and/or other materials provided with the
 *	  distribution.
 *
 *	* Neither the name of Texas Instruments Incorporated nor the names of
 *	  its contributors may be used to endorse or promote products derived
 *	  from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/poll.h>
#include <time.h>
#include <math.h>

#define NUM_MESSAGES		10
#define DEVICE_NAME		"/dev/rpmsg_pru30"

int main(void)
{
	struct pollfd pollfds;
	int i, j;
	float elapsed_seconds;
	unsigned int reads[NUM_MESSAGES];

	/* Open the rpmsg_pru character device file */
	pollfds.fd = open(DEVICE_NAME, O_RDWR);

	/*
	 * If the RPMsg channel doesn't exist yet the character device
	 * won't either.
	 * Make sure the PRU firmware is loaded and that the rpmsg_pru
	 * module is inserted.
	 */
	if (pollfds.fd < 0) 
	{
		printf("Failed to open %s\n", DEVICE_NAME);
		return -1;
	}

	// initial "kick", all writes are dummy data
	write(pollfds.fd, &reads[0], sizeof(reads[0]));

	for (i = 0; i < NUM_MESSAGES; i++) 
	{
		read(pollfds.fd, &reads[i], sizeof(reads[0]));
		write(pollfds.fd, &reads[0], sizeof(reads[0]));
	}

	for(i = 0; i < NUM_MESSAGES; i++)
	{
		elapsed_seconds = (5 * (float)reads[i]) / pow(10, 9);
		printf("PRU elapsed cycles: %u cycles\n", reads[i]);
		printf("PRU elapsed time: %0.9f seconds\n\n", elapsed_seconds);
	}

	/* Close the rpmsg_pru character device file */
	close(pollfds.fd);

	return 0;
}

