#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <ctype.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/mman.h>

#define MAP_SIZE 4096UL
#define MAP_MASK (MAP_SIZE -1)

int main()
{
    int fd;
    void *map_base, *virt_addr;
    unsigned long cycle_count, count_enable, cycle_count_addr, *ctrl;
    off_t ctrl_base = 0x4a324000;
    off_t cycle_count_offset = 0xC;
    cycle_count_addr = ctrl_base + cycle_count_offset;

    if((fd = open("/dev/mem", O_RDWR | O_SYNC)) == -1)
    {
        printf("Failed to open memory!\n");
        return -1;
    }    
    fflush(stdout); // ^\_('')_/^

    map_base = mmap(0, MAP_SIZE , PROT_READ | PROT_WRITE, MAP_SHARED, fd, ctrl_base& ~MAP_MASK);

    if(map_base == (void *) -1)
    {
       printf("Failed to map base address\n");
       return -1;
    }
    fflush(stdout);    

    virt_addr = map_base + (ctrl_base & MAP_MASK);
    ctrl = (unsigned long *)virt_addr;
    printf("Virt addr: %X\n", *ctrl);

    for(int i = 0;i < 5; i++)
    {
        virt_addr = map_base + (cycle_count_addr  & MAP_MASK);
        cycle_count = *((unsigned long *) virt_addr);
        printf("Value of cycle count: %X\n", cycle_count);
        usleep(100);
    }

    if(munmap(map_base, MAP_SIZE) == -1) 
    {
       printf("Failed to unmap memory");
       return -1;
    }

    close(fd);
    return 0;
}
