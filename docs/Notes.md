# Useful Links

- [RT-PREEMPT](https://wiki.linuxfoundation.org/realtime/documentation/howto/applications/preemptrt_setup)
- [ADCPro](http://www.ti.com/tool/ADCPRO?DCMP=hpa_tools&HQS=adcpro)
- [ADS1278 Driver for ADC Pro](https://www.ti.com/licreg/productdownload.tsp?toPerform=productDownload&location=http://software-dl-1.ti.com/download/hpa_pa/6BGXZFYUTJ@3NRKQCQ5ZZZE2BB4XDBK5/ADS1278EVM-PDK-SW-installer.exe)
- [Build BBB Image](https://www.digikey.com/eewiki/display/linuxonarm/BeagleBone+Black)

## Compiling BBB Image
Use arm-linux-gnueabihf- from debian's crossbuild-essential for armhf
You'll have to get autotools also
From Robert Nelson memo, use ti-linux-kernel-dev 4.19 (RT) and the U-Boot version he uses
Valid PRU firmware is at /opt/scripts/device/bone/pru-rpmsg_client_sample. Copy to /lib/firmware for sanity check that PRU works (echo start > state)
If U-Boot won't boot, Ctrl-C a bunch of times then `mmc erase 0 0x7ffff`. Try 1 instead of 0 on error

## Compiling ptpd
1. checkout master
1. `apt install crossbuild-essential-armhf`
1. in configure.ac file, comment out line `AC_FUNC_MALLOC`
1. `autoreconf -vi` if it errors, run it again and should clear up
1. `./configure --host=arm-linux-gnueabihf --disable-statistics --enable-slave-only`
1. `make`

## BBB Image USB Ethernet

```
# Ethernet/RNDIS gadget (g_ether)
# Used by: /opt/scripts/boot/autoconfigure_usb0.sh
iface usb0 inet static
    address 192.168.7.2
    netmask 255.255.255.252
    network 192.168.7.0
    gateway 192.168.7.1
```

## ptpd2
Unit file goes to `/lib/systemd/system/` and `/etc/systemd/system/`
Lots of switches, make sure to change interface to eth0 from usb0 when running for real

## BBB PRU GPIO pins

|  PRU  | R30(output) bit | Pinmux Mode | R31(input) bit | Pinmux Mode | BB Header | BB Pin Name |  GPIO  |
| :---: | :-------------: | :---------: | :------------: | :---------: | :-------: | :---------: | :----: |
|   0   |        0        |      5      |       0        |      6      |   P9_31   |  SPI1_SCLK  |  110   |
|   0   |        1        |      5      |       1        |      6      |   P9_29   |   SPI1_D0   |  111   |
|   0   |        2        |      5      |       2        |      6      |   P9_30   |   SPI1_D1   |  112   |
|   0   |        3        |      5      |       3        |      6      |   P9_28   |  SPI1_CS0   |  113   |
|   0   |        4        |      5      |       4        |      6      |   P9_42   |  (*note1)   | 114/7  |
|   0   |        5        |      5      |       5        |      6      |   P9_27   |  GPIO3_19   |  115   |
|   0   |        6        |      5      |       6        |      6      |   P9_41   |  (*note2)   | 116/20 |
|   0   |        7        |      5      |       7        |      6      |   P9_25   |  GPIO3_21   |  117   |
|   0   |       14        |      6      |      N/A       |             |   P8_12   |  GPIO1_12   |   44   |
|   0   |       15        |      6      |      N/A       |             |   P8_11   |  GPIO1_13   |   45   |
|   0   |       N/A       |             |       14       |      6      |   P8_16   |  GPIO1_14   |   46   |
|   0   |       N/A       |             |       15       |      6      |   P8_15   |  GPIO1_15   |   47   |
|   0   |       N/A       |             |       16       |      6      |   P9_24   |  UART1_TXD  |   15   |
|       |                 |             |                |             |           |             |        |
|   1   |        0        |      5      |       0        |      6      |   P8_45   |   GPIO2_6   |   70   |
|   1   |        1        |      5      |       1        |      6      |   P8_46   |   GPIO2_7   |   71   |
|   1   |        2        |      5      |       2        |      6      |   P8_43   |   GPIO2_8   |   72   |
|   1   |        3        |      5      |       3        |      6      |   P8_44   |   GPIO2_9   |   73   |
|   1   |        4        |      5      |       4        |      6      |   P8_41   |  GPIO2_10   |   74   |
|   1   |        5        |      5      |       5        |      6      |   P8_42   |  GPIO2_11   |   75   |
|   1   |        6        |      5      |       6        |      6      |   P8_39   |  GPIO2_12   |   76   |
|   1   |        7        |      5      |       7        |      6      |   P8_40   |  GPIO2_13   |   77   |
|   1   |        8        |      5      |       8        |      6      |   P8_27   |  GPIO2_22   |   86   |
|   1   |        9        |      5      |       9        |      6      |   P8_29   |  GPIO2_23   |   87   |
|   1   |       10        |      5      |       10       |      6      |   P8_28   |  GPIO2_24   |   88   |
|   1   |       11        |      5      |       11       |      6      |   P8_30   |  GPIO2_25   |   89   |
|   1   |       12        |      5      |       12       |      6      |   P8_21   |  GPIO1_30   |   62   |
|   1   |       13        |      5      |       13       |      6      |   P8_20   |  GPIO1_31   |   63   |
|   1   |       N/A       |             |       16       |      6      |   P9_26   |  UART1_RXD  |   14   |

*Note1: The PRU0 Registers{30,31} Bit 4 (GPIO3_18) is routed to P9_42-GPIO0_7 pin.  You MUST set GPIO0_7 to input mode in pinmuxing.

*Note2: The PRU0 Registers{30,31} Bit 6 (GPIO3_20) is routed to P9_41-GPIO0_20(CLKOUT2). You must set GPIO0_20 to input mode in pinmuxing.

- P8_45 pruin  # ~DRDY
- P8_46 pruout # ~SYNC
- P8_43 pruout # SCLK
- P8_44 pruin  # D1
- P8_41 pruin  # D2 
- P8_42 pruin  # D3
- P8_39 pruin  # D4
- P8_40 pruin  # D5
- P8_27 pruin  # D6
- P8_29 pruin  # D7
- P8_28 pruin  # D8


![Beaglebone Pinout](./pics/Beaglebone-Black-Pinout.png)

- PRU shared mem offset on host side: 0x4a30 0000
- Host shared mem offset on PRU side: 0x0001 0000 

## S2 switch settings
- M1 LO, M0 HI (High Resolution Mode, 01)
- CLKDIV HI (needed for High Res Mode 1)
- F2 LO, F1 HI, F0 LO (SPI, parallel output)

## PRU algorithm
### PRU1
- Set SCLK lo and clear out buffer pointer
- Wait for DRDY line to go lo
- write buffer location for PRU0 to get timestamp for
- shift in data
- write data to buffer and signal PRU0 that data is ready by writing buffer location
### PRU0
- 