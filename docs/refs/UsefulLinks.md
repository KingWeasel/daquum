# Useful Links

- [RT-PREEMPT](https://wiki.linuxfoundation.org/realtime/documentation/howto/applications/preemptrt_setup)
- [ADCPro](http://www.ti.com/tool/ADCPRO?DCMP=hpa_tools&HQS=adcpro)
- [ADS1278 Driver for ADC Pro](https://www.ti.com/licreg/productdownload.tsp?toPerform=productDownload&location=http://software-dl-1.ti.com/download/hpa_pa/6BGXZFYUTJ@3NRKQCQ5ZZZE2BB4XDBK5/ADS1278EVM-PDK-SW-installer.exe)
